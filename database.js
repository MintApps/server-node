/**
 * Functions for accessing the database and managing a pool of connections
 * @author Thomas Kippenberg
 * @module database
*/

import mariadb from 'mariadb'
import config from './config.json' with { type: 'json' }
import { debug } from 'console'

/**
 * pool configuration
 * @see config.js
 */
const pool = mariadb.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  connectionLimit: 10
})
setInterval(cleanUpDatabase, 50000)

/**
 * Regularly clean up the database to remove content that is no longer needed
 */
async function cleanUpDatabase () {
  let conn
  try {
    conn = await pool.getConnection()
    await conn.query('delete from captchas where time < ?', [Date.now() - config.captcha.lifetime])
    await conn.query('delete from challenges where time < ?', [Date.now() - config.challenge.lifetime])
    await conn.query('delete from rooms where (changetime + lifetime) < ? and lifetime > 0', [Date.now()])
    await conn.query('delete from sessions where changetime < ?', [Date.now() - config.session.lifetime])
  } catch (e) {
    debug(e)
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get connection from pool (used by web server)
 * @returns {PoolConnection} - database connection
 */
export async function getConnection () {
  debug(`database pool size ${pool.activeConnections()}`)
  return await pool.getConnection()
}
