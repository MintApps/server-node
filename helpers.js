/**
 * Collection of small auxiliary routines
 * @author Thomas Kippenberg
 * @module helpers
*/

import { randomInt } from 'node:crypto'

/**
 * Get and validate text parameter
 * @param {String} param - parameter value
 * @param {RegExp} regex - regular expression to validate parameter
 * @param {String} [defaultValue=null] - default value (used when parameter is missing)
 * @returns {String} parameter or defaultValue
 */
export function getTextParam (param, regex, defaultValue = null) {
  if (param === undefined || param === null || typeof param !== 'string') return defaultValue
  return param.match(regex) ? param : defaultValue
}

/**
 * Get and validate integer parameter
 * @param {Number} param - parameter value
 * @param {Number} min - minimum value
 * @param {Number} max - maximum value
 * @param {Number} defaultValue - default value (used when parameter is missing)
 * @returns {Number} parameter or defaultValue
 */
export function getIntegerParam (param, min, max, defaultValue) {
  if (param === undefined || param === null) return defaultValue
  if (typeof param === 'string') param = Number.parseInt(param)
  if (isNaN(param)) return defaultValue
  if (param > max) return max
  if (param < min) return min
  return param
}

/**
 * Create random token of letters and digits
 * @param {Number} length - length of token
 * @returns {String} token
 */
export function getRandomToken (length) {
  const characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
  let token = ''
  for (let i = 0; i < length; i++) {
    token += characters[randomInt(characters.length)]
  }
  return token
}
