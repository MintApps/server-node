/**
 * Constants and regular expressions required in the MintApps sync server, especially for validating inputs and messages
 * @author Thomas Kippenberg
 * @module consts
*/

export default {
  // room name (optional)
  ROOM_NAME_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{1,1000}$/gu,
  // room type (optional)
  ROOM_TYPE_REGEX: /^[-0-9a-zA-Z]{1,100}$/,
  // room id (db key)
  ROOM_ID_REGEX: /^[0-9a-zA-Z]{12}$/,
  // room token (generated automatically, user and room specific)
  ROOM_TOKEN_REGEX: /^[0-9a-zA-Z]{1,100}$/,
  // maximum length of single message in characters
  MESSAGE_MAX_LENGTH: 500000,
  // captcha code
  CAPTCHA_REGEX: /^[0-9a-zA-Z]{1,32}$/,
  // challenge
  CHALLENGE_REGEX: /^[0-9]:[a-zA-Z0-9]{10}:[0-9]{10,20}:[0-9]{1,100}$/,
  // users id (db key)
  USER_ID_REGEX: /^[0-9a-zA-Z@_.-]{1,64}$/,
  // user desc (only for accounts)
  USER_DESCRIPTION_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{1,300}$/gu,
  // totp token regex
  USER_TOTP_REGEX: /^[0-9]{1,32}$/,
  // session regex
  SESSION_REGEX: /^[0-9a-zA-Z]{64}$/,
  // keep alive interval for websockets in ms
  WS_PING_INTERVAL: 50000,
  // activities
  ACTIVITY_ID_REGEX: /^[0-9a-zA-Z.+=]{1,64}$/,
  ACTIVITY_TITLE_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{1,300}$/gu,
  ACTIVITY_DESC_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}\n\t\r]{0,500}$/gu,
  ACTIVITY_SUBJECT_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{0,300}$/gu,
  ACTIVITY_LOCALE_REGEX: /^[a-zA-Z_-]{0,8}$/,
  ACTIVITY_TYPE_REGEX: /^[a-z]{1,16}$/,
  ACTIVITY_AUTHOR_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{0,100}$/gu,
  ACTIVITY_LICENSE_REGEX: /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]{0,100}$/gu,
  ACTIVITY_ICON_INDEX: /^(data:)([\w/+-]*)(;charset=[\w-]+|;base64){0,1},(.*)/gi,
  // passwords
  PASSWORD_REGEX: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/
}
